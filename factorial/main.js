let num;

do {
    num = prompt("Enter the number to calculate it's factorial: ", num);
}
while (isNaN(+num) || num === null || num === "" || num < 0);

function factorial(num) {
    if (num == 0) {
        return 1;
    }
    return num * factorial(num - 1);
}

alert(`Factorial of ${num} = ` + factorial(num));


