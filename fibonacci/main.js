let n;
do {
    n = +prompt("Enter the number of fibonacci element you want ot calculate in this row: ", n);
}
while (isNaN(n) || n === null || n === "" || n % 1 === n);

alert(`Fibonacci element number ${n} = ` + fibo(0, 1, n));

function fibo(F0, F1, n) {
    if (n < 0) {
        return fibo(F0, F1, n + 2) - fibo(F0, F1, n + 1);
    }
    if (n === 0) {
        return F0;
    }
    if (n === 1) {
        return F1;
    }
    if (n > 1) {
        return fibo(F0, F1, n - 1) + fibo(F0, F1, n - 2);
    }
}


